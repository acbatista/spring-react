CREATE TABLE clientes(
  id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
  nome VARCHAR (100) NOT NULL ,
  cpf VARCHAR (13) NOT NULL,
  email VARCHAR (30),
  cep VARCHAR(8) NOT NULL,
  logradouro VARCHAR (100) NOT NULL,
  bairro VARCHAR (50) NOT NULL,
  cidade VARCHAR (50) NOT NULL,
  uf VARCHAR (2) NOT NULL,
  complemento VARCHAR (40)
)ENGINE = InnoDB DEFAULT CHARACTER SET = utf8;

CREATE TABLE telefones(
  id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
  cliente_id BIGINT,
  tipo VARCHAR (15),
  numero VARCHAR (11),
  FOREIGN KEY (cliente_id) REFERENCES clientes(id)
)ENGINE = InnoDB DEFAULT CHARACTER SET = utf8;


CREATE TABLE emails(
  id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
  cliente_id BIGINT,
  email VARCHAR (30),
  FOREIGN KEY (cliente_id) REFERENCES clientes(id)
)


INSERT INTO clientes (nome, cpf, email, cep, logradouro, bairro, cidade, uf) values ('João Silva', '00000000000', 'joao@email.com','38400121', 'Casa do Joao','Sul', 'Cidade do Joao','JS');
INSERT INTO telefones(cliente_id, tipo, numero) value (1,"RESIDENCIAL","32322121");
INSERT INTO telefones(cliente_id, tipo, numero) value (1,"CELULAR","999999999");
INSERT INTO emails(cliente_id, email) VALUE (1,"joao@email.com");
INSERT INTO emails(cliente_id, email) VALUE (1,"joao_contatos@email.com");