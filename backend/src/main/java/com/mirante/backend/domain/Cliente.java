package com.mirante.backend.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "clientes")
public class Cliente {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Size(min = 3,max = 100)
    @Column
    @NotNull
    @Pattern( regexp = "/[a-zA-Z\\u00C0-\\u00FF ]+/i")
    private String nome;

    @Column
    @NotNull
    private String cpf;

    @Embedded
    private Endereco endereco;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "cliente")
    private List<Telefone> telefones;

    @OneToMany(fetch = FetchType.LAZY,mappedBy = "cliente")
    private List<Emails> email;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public List<Telefone> getTelefones() {
        return telefones;
    }

    public void setTelefones(List<Telefone> telefones) {
        this.telefones = telefones;
    }

    public List<Emails> getEmail() {
        return email;
    }

    public void setEmail(List<Emails> email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Cliente{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", cpf='" + cpf + '\'' +
                ", endereco=" + endereco +
                ", telefones=" + telefones +
                ", email=" + email +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cliente cliente = (Cliente) o;
        return Objects.equals(id, cliente.id) &&
                Objects.equals(nome, cliente.nome) &&
                Objects.equals(cpf, cliente.cpf) &&
                Objects.equals(endereco, cliente.endereco) &&
                Objects.equals(telefones, cliente.telefones) &&
                Objects.equals(email, cliente.email);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, nome, cpf, endereco, telefones, email);
    }
}
