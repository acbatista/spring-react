package com.mirante.backend.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mirante.backend.domain.Cliente;
import com.mirante.backend.domain.Tipo;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
@Table(name = "telefones")
public class Telefone {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty
    @Enumerated(EnumType.STRING)
    private Tipo tipo;

    @Email
    @NotNull
    private String numero;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cliente_id")
    @JsonIgnore
    private Cliente cliente;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Tipo getTipo() {
        return tipo;
    }

    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Telefone telefone = (Telefone) o;
        return Objects.equals(id, telefone.id) &&
                tipo == telefone.tipo &&
                Objects.equals(numero, telefone.numero) &&
                Objects.equals(cliente, telefone.cliente);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, tipo, numero, cliente);
    }

    @Override
    public String toString() {
        return "Telefone{" +
                "id=" + id +
                ", tipo=" + tipo +
                ", numero='" + numero + '\'' +
                ", cliente=" + cliente +
                '}';
    }
}
